import './style/bootstrap.scss';
import './style/app.scss';

import React from "react";
import ReactDOM from 'react-dom';
import store from './store/store';
import { Provider, useSelector } from 'react-redux';
import { BrowserRouter as Router, Redirect, Route, Switch } from "react-router-dom";
import LandingPage from './pages/Landing/Landing';
import RegisterPage from './pages/Register/Register';
import LoginPage from './pages/Login/Login';
import NotFoundPage from './pages/NotFound/NotFound';
import LoaderContainer from './components/LoaderContainer/LoaderContainer';
import DashboardPage from './pages/Dashboard/Dashboard';
import TeamsPage from './pages/Teams/Teams';
import TeamCreatePage from './pages/TeamCreate/TeamCreate';
import TeamPage from './pages/Team/Team';
import TeamInvitePage from './pages/TeamInvite/TeamInvite';
import InvitationsPage from './pages/Invitations/Invitations';
import TemplatesPage from './pages/Templates/Templates';
import TemplatePage from './pages/Template/Template';
import TemplateCreatePage from './pages/TemplateCreate/TemplateCreate';
import BoardsPage from './pages/Boards/Boards';
import BoardPage from './pages/Board/Board';
import BoardCreatePage from './pages/BoardCreate/BoardCreate';
import User from "./pages/User/User";

function App() {
    const hasToken = useSelector(state => state.auth.token);

    return <Router>
        {hasToken
            ? <Switch>
                <Redirect push={false} path="/login" to="/" />
                <Redirect push={false} path="/register" to="/" />

                <Route path="/">
                    <LoaderContainer>
                        <Switch>
                            <Route path="/" exact component={DashboardPage} />
                            <Route path="/teams" exact component={TeamsPage} />
                            <Route path="/teams/create" exact component={TeamCreatePage} />
                            <Route path="/teams/invite" exact component={TeamInvitePage} />
                            <Route path="/teams/:teamId" exact component={TeamPage} />

                            <Route path="/user/invitations" exact component={InvitationsPage} />

                            <Route path="/templates" exact component={TemplatesPage} />
                            <Route path="/templates/create" exact component={TemplateCreatePage} />
                            <Route path="/templates/:templateId" exact component={TemplatePage} />

                            <Route path="/boards" exact component={BoardsPage} />
                            <Route path="/boards/create" exact component={BoardCreatePage} />
                            <Route path="/boards/:boardId" exact component={BoardPage} />

                            <Route path="/users/:userId" exact component={User} />

                            <Route component={NotFoundPage} />
                        </Switch>
                    </LoaderContainer>
                </Route>
            </Switch>
            : <Switch>
                <Route path="/" exact component={LandingPage} />
                <Route path="/register" exact component={RegisterPage} />
                <Route path="/login" exact component={LoginPage} />
                <Redirect to="/login" />
            </Switch>
        }
    </Router>;
}

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);