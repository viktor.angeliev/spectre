const TemplateNameValidation = () => ({
    required: "A template name is required.",
    minLength: { value: 2, message: "The template name is too short." },
    maxLength: { value: 30, message: "The template name is too long." },
});

const TemplateColumnsValidation = () => ({
    validate: columns => {
        if (columns.length == 0)
            return 'Add at least one column.';

        let nameRequired = false;

        columns.forEach(column => {
            if (column.name.length == 0)
                nameRequired = true;
        });

        if (nameRequired)
            return 'All columns must have names.';

        return true;
    }
});

export { TemplateColumnsValidation, TemplateNameValidation };