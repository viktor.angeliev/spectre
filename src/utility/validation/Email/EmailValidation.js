export const emailRegex = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

export default () => ({
    required: "An email is required.",
    pattern: { value: emailRegex, message: "You must enter a valid email." }
});