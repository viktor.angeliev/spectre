const isTeamNameUnique = teams => async value => {
    const found = teams.find(team => team.name == value);
    if (found)
        return "The team name must be unique.";

    return true;
};

export default teams => ({
    required: "A team name is required",
        minLength: { value: 2, message: "The team name is too short." },
    maxLength: { value: 30, message: "The team name is too long." },
    validate: isTeamNameUnique(teams)
});