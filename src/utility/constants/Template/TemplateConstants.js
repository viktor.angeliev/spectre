export const colors = ["red", "blue", "purple", "teal", "green"];
export const nextColor = color => color + 1 >= colors.length ? 0 : color + 1;