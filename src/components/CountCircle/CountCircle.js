import React from 'react';
import './CountCircle.scss';

export default function CountCircle({ className, value, ...props }) {
    return <div className={`count-circle ${className ?? ''}`} {...props}>{value}</div>;
}