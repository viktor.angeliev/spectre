import React, { useCallback, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import Icon from '../Icon/Icon';

export default function EditableText({ text, onSave, wrapper, validation = {}, ...props }) {
    const { register, watch, setValue, handleSubmit, clearErrors, formState: { errors } } = useForm({ reValidateMode: 'onSubmit' });
    const [isEditing, setIsEditing] = useState(false);
    const watchText = watch('text');

    const startEdit = useCallback(() => {
        setValue('text', text);
        setIsEditing(true);
    }, [setValue, text]);

    const onSubmit = useCallback(data => {
        setIsEditing(false);
        onSave(data);
    }, [onSave]);

    const onCancel = useCallback(() => {
        clearErrors();
        setIsEditing(false);
    }, [clearErrors]);

    const Wrapper = wrapper.type;

    return <div {...props}>
        <Wrapper {...wrapper.props}>
            {isEditing &&
                <Form className="d-flex flex-wrap justify-content-center" onSubmit={handleSubmit(onSubmit)}>
                    <div className="w-auto d-flex gap-2">
                        <Form.Control {...register("text", { ...validation })} />
                        <Button variant="primary" type="submit" disabled={text === watchText}>Save</Button>
                        <Button variant="danger" onClick={onCancel}>Cancel</Button>
                        {errors.text && <span className="fs-6 text-danger fst-italic w-100">{errors.text.message}</span>}
                    </div>
                </Form>
            }

            {!isEditing && text}
            {!isEditing && <span onClick={startEdit}><Icon.Edit /></span>}
        </Wrapper>
    </div>;
}