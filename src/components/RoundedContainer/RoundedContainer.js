import React from 'react';

export default function RoundedContainer({children, className, tag, ...props}) {
    const Tag = tag??'div';
    return <Tag className={`border rounded bg-white p-2 mt-2 ${className??''}`} {...props}>
        {children}
    </Tag>;
}