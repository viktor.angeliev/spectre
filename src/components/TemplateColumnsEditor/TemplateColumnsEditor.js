import React, { useCallback } from 'react';
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import { FormControl } from 'react-bootstrap';
import { colors, nextColor } from '../../utility/constants/Template/TemplateConstants';
import Icon from '../Icon/Icon';
import RowButton from '../RowButton/RowButton';
import './TemplateColumnsEditor.scss';


export default function TemplateColumnsEditor({ columns, setColumns }) {
    const onDragEnd = useCallback(result => {
        const { destination, source, draggableId } = result;

        if (!destination)
            return;
        if (destination.droppableId === source.droppableId && destination.index === source.index)
            return;

        const column = columns[draggableId];
        let newColumns = [...columns];

        newColumns.splice(source.index, 1);
        newColumns.splice(destination.index, 0, column);
        newColumns = newColumns.map((column, index) => ({ ...column, id: index }));

        setColumns(newColumns);
    }, [columns, setColumns]);

    const onClickAddColumn = useCallback(() => {
        let newColumns = [
            {
                name: "Column " + (columns.length + 1),
                color: columns.length % (colors.length)
            },
            ...columns
        ];
        //Update indexes
        newColumns = newColumns.map((column, index) => ({ ...column, id: index }));
        setColumns(newColumns);
    }, [columns, setColumns]);

    const onClickColumnColor = useCallback(index => {
        const newColumns = [...columns];
        const newColor = nextColor(columns[index].color);
        //Deep clone column object and apply new color
        const column = { ...newColumns[index], color: newColor };
        newColumns[index] = column;

        setColumns(newColumns);
    }, [columns, setColumns]);

    const onChangeColumnName = useCallback((index, e) => {
        const newColumns = [...columns];
        //Deep clone column object and apply name
        const column = { ...newColumns[index], name: e.target.value };
        newColumns[index] = column;

        setColumns(newColumns);
    }, [columns, setColumns]);

    const onClickDeleteColumn = useCallback(index => {
        let newColumns = [...columns];
        newColumns.splice(index, 1);
        //Update indexes
        newColumns = newColumns.map((column, index) => ({ ...column, id: index }));
        setColumns(newColumns);
    }, [columns, setColumns]);

    return <React.Fragment>
        <RowButton tag="div" className="mt-0" icon={Icon.Add} noArrow onClick={onClickAddColumn}>
            Add column
        </RowButton>
        <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId={'columns'}>
                {provided =>
                    <div ref={provided.innerRef} {...provided.droppableProps}>
                        {columns.map((column, index) =>
                            <Column
                                key={column.id}
                                column={column}
                                index={index}
                                onClickColumnColor={onClickColumnColor}
                                onChangeColumnName={onChangeColumnName}
                                onClickDeleteColumn={onClickDeleteColumn}
                            />
                        )}
                        {provided.placeholder}
                    </div>
                }
            </Droppable>
        </DragDropContext>
    </React.Fragment>;
}


function Column({ column, index, onClickDeleteColumn, onClickColumnColor, onChangeColumnName }) {
    return <Draggable draggableId={column.id + ''} index={index}>
        {(provided, snapshot) => {
            return <div
                className={`template-column ${snapshot.isDragging ? 'template-column-dragging' : ''}`}
                ref={provided.innerRef}
                {...provided.draggableProps}
            >
                <div
                    className="template-column-handle"
                    {...provided.dragHandleProps}
                >
                    <Icon.Drag />
                </div>
                <div
                    className={`template-column-color tc-${colors[column.color]}`}
                    onClick={() => onClickColumnColor(index)}
                ></div>
                <div className="template-column-name">
                    <FormControl value={column.name} onChange={e => onChangeColumnName(index, e)} maxLength="20" />
                </div>
                <div className="template-column-delete" onClick={() => onClickDeleteColumn(index)}>
                    <Icon.Trash />
                </div>
            </div>;
        }}
    </Draggable>;
}