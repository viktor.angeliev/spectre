import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { load, unload } from '../../store/slices/loader/loaderSlice';
import LoadingPage from '../../pages/Loading/Loading';

export default function LoaderContainer({ children }) {
    const dispatch = useDispatch();
    const isLoaded = useSelector(state => state.loader.isLoaded);

    useEffect(() => {
        //Load data when that component is mounted - after a login or after a page reload
        dispatch(load());
        //Unload data when the component is unmounted - after a logout
        return () => dispatch(unload());
    }, [dispatch]);

    if (!isLoaded)
        return <LoadingPage />;

    return <React.Fragment>{children}</React.Fragment>;
}