import React from 'react';
import { Link } from 'react-router-dom';
import './RowButton.scss';

export default function RowButton({ className, children, tag, icon, noArrow, ...props }) {
    const Tag = tag ?? Link;
    const Icon = icon;
    return <Tag className={`row_button ${className ?? ''} ${noArrow ? '' : 'row-button-arrow'}`} {...props}>
        {icon && <div className='row_button-icon'><Icon /></div>}
        <div className="row-button-content">
            {children}
        </div>
    </Tag>;
}