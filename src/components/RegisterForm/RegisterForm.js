import React, { useCallback, useEffect } from "react";
import { Button, Form, Row } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { register as registerReducer, clearErrors } from "../../store/slices/auth/authSlice";
import EmailValidation from "../../utility/validation/Email/EmailValidation";

export default function RegisterForm() {
    const dispatch = useDispatch();
    const history = useHistory();
    const { register, handleSubmit, formState: { errors } } = useForm();
    const registerError = useSelector(state => state.auth.errors.register);

    useEffect(() => {
        dispatch(clearErrors());
    }, [dispatch]);

    const onSubmit = useCallback(formData => {
        const data = {
            fullName: formData.firstName + ' ' + formData.lastName,
            email: formData.email,
            password: formData.password
        };

        dispatch(registerReducer(data)).then(payload => {
            if (payload.error)
                return;

            history.push('/');
        });
    }, [dispatch, history]);

    return <Form onSubmit={handleSubmit(onSubmit)}>
        <h5 className="text-danger">{registerError}</h5>

        <Row className="mt-3">
            <Form.Group className="mb-3 col-sm-6 col-12">
                <Form.Label htmlFor="email">First name: *</Form.Label>
                <Form.Control
                    className={errors.firstName && 'border-danger'}
                    {...register("firstName", { required: true })}
                    autoComplete="firstName"
                />
            </Form.Group>
            <Form.Group className="mb-3 col-sm-6 col-12">
                <Form.Label htmlFor="email">Last name: *</Form.Label>
                <Form.Control
                    className={errors.lastName && 'border-danger'}
                    {...register("lastName", { required: true })}
                    autoComplete="lastName"
                />
            </Form.Group>
        </Row>

        <Form.Group className="my-3">
            <Form.Label htmlFor="email">
                Email: *
                {errors.email && <span className="text-danger fst-italic m-0"> {errors.email.message}</span>}
            </Form.Label>
            <Form.Control
                className={errors.email && 'border-danger'}
                {...register("email", EmailValidation())}
                autoComplete="email"
            />
        </Form.Group>

        <Form.Group className="my-3">
            <Form.Label htmlFor="password">Password: * </Form.Label>
            <Form.Control
                className={errors.password && 'border-danger'}
                {...register("password", { required: true })}
                type="password"
                autoComplete="password"
            />
        </Form.Group>

        <Button className="w-100" variant="primary" type="submit">Register</Button>
    </Form>;
}