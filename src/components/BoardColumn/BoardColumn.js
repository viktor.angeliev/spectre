import React, { useCallback, useState } from 'react';
import { Form } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { addCardRating, createCard, deleteCard, removeCardRating } from '../../store/slices/boards/boardsSlice';
import { colors } from '../../utility/constants/Template/TemplateConstants';
import Icon from '../Icon/Icon';
import FlipMove from 'react-flip-move';

export default function BoardColumn({ board, column }) {
    const dispatch = useDispatch();
    const [isAdding, setIsAdding] = useState(false);
    const [value, setValue] = useState('');

    const onInputKeyDown = useCallback(e => {
        if (e.key === 'Enter') {
            if (value.trim().length == 0)
                return;

            const data = {
                column: column.id,
                boardId: board.id,
                text: value
            };
            dispatch(createCard(data)).then(() => {
                setIsAdding(false);
                setValue('');
            });
        }

        if (e.key === 'Escape') {
            setIsAdding(false);
            setValue('');
        }
    }, [board, column, dispatch, value]);

    const onInputBlur = useCallback(() => {
        setIsAdding(false);
        setValue('');
    }, []);

    const onClickDelete = useCallback(id => {
        dispatch(deleteCard(id));
    }, [dispatch]);

    const onClickCardUp = useCallback(id => {
        dispatch(addCardRating(id));
    }, [dispatch]);

    const onClickCardDown = useCallback(id => {
        dispatch(removeCardRating(id));
    }, [dispatch]);

    return <div key={column.id} className={`board-column tc-${colors[column.color]}`}>
        <div className={`board-column-name`}>
            {column.name}
        </div>
        <div className="board-column-card-container">
            {!isAdding &&
                <div className="board-card-add-button" onClick={() => setIsAdding(true)}>
                    <Icon.Add />
                </div>
            }
            {isAdding &&
                <div className="board-card-add-input">
                    <Form.Control
                        size="sm"
                        type="text"
                        autoFocus
                        onKeyDown={onInputKeyDown}
                        onBlur={onInputBlur}
                        onChange={e => setValue(e.target.value)}
                        value={value}
                    />
                </div>
            }
            <FlipMove>
                {board.cards.filter(card => card.column == column.id).map(card =>
                    <div className="board-card" key={card.id}>
                        <div className="board-card-text">
                            {card.text}
                        </div>
                        <div className="board-card-footer">
                            <span className="fst-italic">{card.user.fullName}</span>
                            <span className="flex-grow-1 text-center">
                                <Icon.Remove onClick={() => onClickCardDown(card.id)} />
                                {card.rating}
                                <Icon.Add onClick={() => onClickCardUp(card.id)} />
                            </span>
                            <Icon.Trash className="board-card-delete" onClick={() => onClickDelete(card.id)} />
                        </div>
                    </div>
                )}
            </FlipMove>
        </div>
    </div>;
}