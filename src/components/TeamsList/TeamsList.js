import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useRouteMatch } from 'react-router-dom';
import { getTeams } from '../../store/slices/teams/teamsSlice';
import Icon from '../Icon/Icon';
import RowButton from '../RowButton/RowButton';

export default function TeamsList() {
    const { url } = useRouteMatch();
    const dispatch = useDispatch();
    const teams = useSelector(state => state.teams.all);

    useEffect(() => {
        dispatch(getTeams());
    }, [dispatch]);

    return <div>
        {teams &&
            teams.map(team =>
                <RowButton key={team.id} to={`${url}/${team.id}`} icon={Icon.Group}>
                    {team.name}
                </RowButton>
            )}
    </div>;
}