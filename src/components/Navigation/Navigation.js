import React, { useCallback } from 'react';
import { Container, Nav, Navbar, NavDropdown, NavLink } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { logout } from '../../store/slices/auth/authSlice';
import './Navigation.scss';

export function Navigation() {
    const history = useHistory();
    const dispatch = useDispatch();
    const user = useSelector(state => state.auth.user);
    const joined = useSelector(state => state.teams.joined);

    const onClickLogout = useCallback(() => {
        dispatch(logout());
        history.push('/');
    }, [history, dispatch]);

    return <Container>
        <Navbar bg="white" expand="lg" className="border rounded px-5 py-1 mt-2">
            <Navbar.Brand><Link to="/" className="ff-sonsie text-primary text-decoration-none">Spectre</Link></Navbar.Brand>
            <Navbar.Toggle aria-controls="navbar-nav" />
            <Navbar.Collapse className="navbar-top-border">
                <Nav className="w-100">
                    <NavLink as={Link} to="/">Dashboard</NavLink>
                    <NavLink as={Link} to="/teams">Teams</NavLink>
                    <NavLink as={Link} to="/templates">Templates</NavLink>
                    <NavLink as={Link} to="/boards">Boards</NavLink>

                    <NavDropdown className="ms-auto d-none d-lg-block align-self-center" menuVariant="light" title={user.fullName}>
                        <NavDropdown.Item as={Link} to="/user">Profile</NavDropdown.Item>

                        {!joined &&
                            <NavDropdown.Item as={Link} to="/user/invitations">Invitations</NavDropdown.Item>
                        }
                        {joined &&
                            <NavDropdown.Item as={Link} to={`/teams/${joined.id}`}>{joined.name}</NavDropdown.Item>
                        }
                        <NavDropdown.Divider />
                        <NavDropdown.Item href="#" onClick={onClickLogout}>Log out</NavDropdown.Item>
                    </NavDropdown>
                    
                    <hr className="d-block d-lg-none" />
                    <NavLink as="span" className="d-block d-lg-none fw-bold">{user.fullName}</NavLink>
                    <NavLink as={Link} className="d-block d-lg-none" to="/user">Profile</NavLink>
                    {!joined &&
                        <NavLink as={Link} className="d-lg-none" to="/user/invitations">Invitations</NavLink>
                    }
                    {joined &&
                        <NavLink as={Link} className="d-lg-none" to={`/teams/${joined.id}`}>{joined.name}</NavLink>
                    }
                    <a className="nav-link d-block d-lg-none" href="#" onClick={onClickLogout}>Log out</a>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    </Container>;
}