import React, { useCallback, useEffect } from "react";
import { Form, Button } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { login, clearErrors } from "../../store/slices/auth/authSlice";
import EmailValidation from "../../utility/validation/Email/EmailValidation";

export default function LoginForm() {
    const dispatch = useDispatch();
    const history = useHistory();

    const { register, handleSubmit, formState: { errors } } = useForm();

    const loginError = useSelector(state => state.auth.errors.login);

    useEffect(() => {
        dispatch(clearErrors());
    }, [dispatch]);

    const onSubmit = useCallback(data => {
        dispatch(login(data)).then(payload => {
            if (payload.error)
                return;

            history.push('/');
        });
    }, [dispatch, history]);

    return <Form onSubmit={handleSubmit(onSubmit)}>
        <h5 className="text-danger">{loginError}</h5>

        <Form.Group className="my-3">
            <Form.Label htmlFor="email">
                Email:
                {errors.email && <span className="text-danger fst-italic m-0"> {errors.email.message}</span>}
            </Form.Label>
            <Form.Control
                className={errors.email && 'border-danger'}
                {...register("email", EmailValidation())}
                autoComplete="email"
            />
        </Form.Group>

        <Form.Group className="my-3">
            <Form.Label htmlFor="password">Password:</Form.Label>
            <Form.Control
                className={errors.password && 'border-danger'}
                {...register("password", { required: true })}
                type="password"
                autoComplete="password"
            />
        </Form.Group>

        <Button className="w-100" variant="primary" type="submit">Login</Button>
    </Form>;
}