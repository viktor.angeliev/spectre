import React from 'react';

const icon = src => ({ ...params }) => {
    return <img src={`/assets/icons/${src}`} {...params} width="24" height="24" />;
};

export default {
    Group: icon("groups_black_24dp.svg"),
    Add: icon("add_black_24dp.svg"),
    Remove: icon("remove_black_24dp.svg"),
    Edit: icon("edit_black_24dp.svg"),
    Person: icon("person_black_24dp.svg"),
    Columns: icon("columns_black_24dp.svg"),
    Drag: icon("drag_indicator_black_24dp.svg"),
    Trash: icon("delete_outline_black_24dp.svg"),
    Board: icon("developer_board_black_24dp.svg"),
    ThumbUp: icon("thumb_up_black_24dp.svg"),
    ThumbDown: icon("thumb_down_black_24dp.svg"),
};