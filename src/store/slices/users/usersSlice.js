import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    all: [],
    user: null
};

export const getAllUsers = createAsyncThunk(
    'users/getAllUsers',
    async (data, thunkAPI) => {
        try {
            const response = await axios.get('/api/users');
            return response.data;
        } catch (error) {
            return thunkAPI.rejectWithValue(false);
        }
    }
);

export const getUser = createAsyncThunk(
    'users/getUser',
    async (id, thunkAPI) => {
        try {
            const response = await axios.get('/api/users/' + id);
            return response.data;
        } catch (error) {
            return thunkAPI.rejectWithValue(false);
        }
    }
);

export const usersSlice = createSlice({
    name: 'users',
    initialState: initialState,
    extraReducers: builder => {
        builder.addCase(getAllUsers.fulfilled, (state, action) => {
            state.all = action.payload;
        });
        builder.addCase(getUser.fulfilled, (state, action) => {
            state.user = action.payload;
        });
    }
});

export default usersSlice.reducer;