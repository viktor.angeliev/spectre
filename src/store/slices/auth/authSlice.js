import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import store from '../../store';

const initialState = {
    isAuth: false,
    token: null,
    user: null,
    errors: { register: '', login: '' }
};

export const register = createAsyncThunk(
    'auth/register',
    async (data, thunkAPI) => {
        try {
            const response = await axios.post('/api/users', data);
            return response.data;
        } catch (error) {
            return thunkAPI.rejectWithValue(error.response.data);
        }
    }
);

export const login = createAsyncThunk(
    'auth/login',
    async (data, thunkAPI) => {
        try {
            const response = await axios.post('/api/login', data);
            return response.data;
        } catch (error) {
            return thunkAPI.rejectWithValue(error.response.data);
        }
    }
);

export const reauthenticate = createAsyncThunk(
    'auth/reauthenticate',
    async (token, thunkAPI) => {
        try {
            const response = await axios.post('/api/reauthenticate', { auth_token: token });
            return response.data;
        } catch (error) {
            return thunkAPI.rejectWithValue(false);
        }
    }
);

export const authSlice = createSlice({
    name: 'auth',
    initialState: initialState,
    reducers: {
        clearErrors: state => {
            state.errors = { register: '', login: '' };
        },
        logout: state => {
            state.isAuth = false;
            state.token = null;
            state.user = null;
        }
    },
    extraReducers: builder => {
        builder.addCase(register.fulfilled, (state, action) => {
            state.token = action.payload.accessToken;
        });
        builder.addCase(register.rejected, (state, action) => {
            state.errors.register = action.payload;
        });

        builder.addCase(login.fulfilled, (state, action) => {
            state.token = action.payload.accessToken;
        });
        builder.addCase(login.rejected, (state, action) => {
            state.errors.login = action.payload;
        });

        builder.addCase(reauthenticate.fulfilled, (state, action) => {
            state.isAuth = true;
            state.user = action.payload;
        });
        builder.addCase(reauthenticate.rejected, state => {
            state.token = null;
        });
    }
});

//Handle invalid token or token expiry
axios.interceptors.response.use(response => response, error => {
    if (error.response.status === 401)
        store.dispatch(logout());

    return Promise.reject(error);
});

export const authMiddleware = () => next => action => {
    if (action.type === "auth/login/fulfilled"
        || action.type === "auth/register/fulfilled") {
        const token = action.payload.accessToken;
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
        localStorage.setItem('auth_token', token);
    }

    if (action.type === "auth/logout") {
        axios.defaults.headers.common['Authorization'] = '';
        localStorage.removeItem('auth_token');
    }
    return next(action);
};

export const loadAuth = () => {
    const token = localStorage.getItem('auth_token');

    if (token) {
        try {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            return { ...initialState, token: token };
        } catch (err) {
            return undefined;
        }
    }

    return undefined;
};

export const { clearErrors, logout } = authSlice.actions;
export default authSlice.reducer;
