import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    all: []
};

export const getAllInvitations = createAsyncThunk(
    'invitations/getAllInvitations',
    async (data, thunkAPI) => {
        try {
            const response = await axios.get('/api/invitations?_expand=team&_expand=user');
            return response.data;
        } catch (error) {
            return thunkAPI.rejectWithValue(false);
        }
    }
);

export const createInvitation = createAsyncThunk(
    'invitations/createInvitation',
    async (data, thunkAPI) => {
        try {
            const response = await axios.post('/api/invitations', data);
            const expandResponse = await axios.get(`/api/invitations/${response.data.id}?_expand=team&_expand=user`);
            return expandResponse.data;
        } catch (error) {
            return thunkAPI.rejectWithValue(false);
        }
    }
);

export const deleteInvitation = createAsyncThunk(
    'invitations/deleteInvitation',
    async (id, thunkAPI) => {
        try {
            await axios.delete(`/api/invitations/${id}`);
            return id;
        } catch (error) {
            //If the invitation doesn't exist, delete it from the array
            if (error.response.status == 404)
                return id;

            return thunkAPI.rejectWithValue(false);
        }
    }
);

export const invitationsSlice = createSlice({
    name: 'invitations',
    initialState: initialState,
    extraReducers: builder => {
        builder.addCase(getAllInvitations.fulfilled, (state, action) => {
            state.all = action.payload;
        });
        builder.addCase(createInvitation.fulfilled, (state, action) => {
            state.all = [...state.all, action.payload];
        });
        builder.addCase(deleteInvitation.fulfilled, (state, action) => {
            state.all = state.all.filter(invitaion => invitaion.id != action.payload);
        });
    }
});

export default invitationsSlice.reducer;
