import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { postLogin, postLogout } from "../loader/loaderSlice";

const initialState = {
    all: [],
    joined: null
};

export const getTeams = createAsyncThunk(
    'teams/getTeams',
    async (config, thunkAPI) => {
        try {
            const response = await axios.get('/api/teams?_embed=users', config);
            return response.data;
        } catch (error) {
            return thunkAPI.rejectWithValue(false);
        }
    }
);

export const createTeam = createAsyncThunk(
    'teams/createTeam',
    async (data, thunkAPI) => {
        try {
            const response = await axios.post('/api/teams', data);
            return response.data;
        } catch (error) {
            return thunkAPI.rejectWithValue(false);
        }
    }
);

export const patchTeam = createAsyncThunk(
    'teams/patchTeam',
    async (config, thunkAPI) => {
        try {
            await axios.patch(`/api/teams/${config.id}?_embed=users`, config.data);
            const response = await axios.get(`/api/teams/${config.id}?_embed=users`);
            return response.data;
        } catch (error) {
            return thunkAPI.rejectWithValue(false);
        }
    }
);

export const deleteTeam = createAsyncThunk(
    'teams/deleteTeam',
    async (id, thunkAPI) => {
        try {
            //If deleteing the currently joined team, leave it first
            const joined = thunkAPI.getState().teams.joined;
            if(joined && joined.id == id)
                await thunkAPI.dispatch(leave());

            //Must unset all team members teamId field otherwise json-server deletes the users too
            const response = await axios.get(`/api/users/?teamId=${id}`);
            const users = response.data;
            if(users.length > 0){
                //Wait for all request to finish
                await Promise.all(users.map(user => {
                    user.teamId = undefined;
                    return axios.put(`/api/users/${user.id}`, user);
                }));
            }
            
            await axios.delete(`/api/teams/${id}`);
            return id;
        } catch (error) {
            //If the team doesn't exist, delete it from the array
            if (error.response.status == 404)
                return id;
            return thunkAPI.rejectWithValue(false);
        }
    }
);

export const join = createAsyncThunk(
    'teams/join',
    async (team, thunkAPI) => {
        const state = thunkAPI.getState();
        try {
            await axios.patch(`/api/users/${state.auth.user.id}`, { teamId: team.id });
            thunkAPI.dispatch(getTeams());
            return team;
        } catch (error) {
            return thunkAPI.rejectWithValue(false);
        }
    }
);

export const leave = createAsyncThunk(
    'teams/leave',
    async (data, thunkAPI) => {
        const state = thunkAPI.getState();
        try {
            //Unset teamId field
            const user = {...state.auth.user};
            user.teamId = undefined;
            await axios.put(`/api/users/${state.auth.user.id}`, user);

            thunkAPI.dispatch(getTeams());
            return true;
        } catch (error) {
            return thunkAPI.rejectWithValue(false);
        }
    }
);

export const loadJoined = createAsyncThunk(
    'team/loadJoined',
    async (user, thunkAPI) => {
        if (user.teamId >= 0) {
            try {
                const response = await axios.get(`/api/teams/${user.teamId}`);
                return response.data;
            } catch (error) {
                return thunkAPI.rejectWithValue(false);
            }
        }
        return thunkAPI.rejectWithValue(false);
    }
);

export const teamsSlice = createSlice({
    name: 'teams',
    initialState: initialState,
    reducers: {
        unloadJoined: state => {
            state.joined = null;
        }
    },
    extraReducers: builder => {
        builder.addCase(getTeams.fulfilled, (state, action) => {
            state.all = action.payload;
        });
        builder.addCase(createTeam.fulfilled, (state, action) => {
            state.all = [...state.all, action.payload];
        });
        builder.addCase(patchTeam.fulfilled, (state, action) => {
            const payload = action.payload;
            const index = state.all.findIndex(team => team.id == payload.id);
            if (index >= 0)
                state.all[index] = payload;
        });
        builder.addCase(deleteTeam.fulfilled, (state, action) => {
            const arr = state.all.filter(team => team.id != action.payload);
            state.all = arr;
        });

        builder.addCase(join.fulfilled, (state, action) => {
            state.joined = action.payload;
        });

        builder.addCase(loadJoined.fulfilled, (state, action) => {
            state.joined = action.payload;
        });

        builder.addCase(leave.fulfilled, state => {
            state.joined = null;
        });
    }
});

export const { unloadJoined } = teamsSlice.actions;
export default teamsSlice.reducer;

postLogin.push(loadJoined);
postLogout.push(unloadJoined);