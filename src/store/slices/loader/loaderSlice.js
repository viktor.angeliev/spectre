import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { reauthenticate } from "../auth/authSlice";

const initialState = {
    isLoaded: false
};

//Array of actions to be dispatched after successful login or logout;
export const postLogin = [];
export const postLogout = [];

export const load = createAsyncThunk(
    'loader/loadStatus',
    async (data, thunkAPI) => {
        try {
            const state = thunkAPI.getState();
            if (state.auth.token) {
                //Try to reauthenticate user
                const data = await thunkAPI.dispatch(reauthenticate(state.auth.token));
                const user = data.payload;
                //If authentication was successful
                if (user) {
                    //Call all post login functions
                    const arr = postLogin.map(fn => thunkAPI.dispatch(fn(user)));
                    //Wait for all to finish
                    await Promise.all(arr);
                } else {
                    // Reject if reauthentication was unsuccessfull
                    return thunkAPI.rejectWithValue(false);
                }
            }
            return true;
        } catch (error) {
            return thunkAPI.rejectWithValue(false);
        }
    }
);

export const unload = createAsyncThunk(
    'loader/unloadStatus',
    async (data, thunkAPI) => {
        const arr = postLogout.map(fn => thunkAPI.dispatch(fn()));
        await Promise.all(arr);

        return true;
    }
);
/* 
export const waiter = createAsyncThunk(
    'loader/waiter',
    async () => {
        await new Promise(resolve => setTimeout(resolve, 2000));
        return true;
    }
);
//Waiter will get executed after a login
postLogin.push(waiter);
*/

export const loaderSlice = createSlice({
    name: 'loaderSlice',
    initialState: initialState,
    extraReducers: builder => {
        builder.addCase(load.fulfilled, state => {
            state.isLoaded = true;
        });
        builder.addCase(unload.fulfilled, state => {
            state.isLoaded = false;
        });
    }
});

export default loaderSlice.reducer;