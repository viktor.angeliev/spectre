import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    all: []
};

export const getAllTemplates = createAsyncThunk(
    'templates/getAllTemplates',
    async (data, thunkAPI) => {
        try {
            const response = await axios.get('/api/templates');
            return response.data;
        } catch (error) {
            thunkAPI.rejectWithValue(false);
        }
    }
);

export const createTemplate = createAsyncThunk(
    'templates/createTemplate',
    async (data, thunkAPI) => {
        try {
            const response = await axios.post('/api/templates', data);
            return response.data;
        } catch (error) {
            return thunkAPI.rejectWithValue(false);
        }
    }
);

export const patchTemplate = createAsyncThunk(
    'templates/patchTemplate',
    async (config, thunkAPI) => {
        try {
            await axios.patch(`/api/templates/${config.id}?`, config.data);
            const response = await axios.get(`/api/templates/${config.id}?`);
            return response.data;
        } catch (error) {
            return thunkAPI.rejectWithValue(false);
        }
    }
);

export const deleteTemplate = createAsyncThunk(
    'templates/deleteTemplate',
    async (id, thunkAPI) => {
        try {
            await axios.delete(`/api/templates/${id}`);
            return id;
        } catch (error) {
            //If the template doesn't exist, delete it from the array
            if (error.response.status == 404)
                return id;
            return thunkAPI.rejectWithValue(false);
        }
    }
);

export const templatesSlice = createSlice({
    name: 'templates',
    initialState: initialState,
    extraReducers: builder => {
        builder.addCase(getAllTemplates.fulfilled, (state, action) => {
            state.all = action.payload;
        });
        builder.addCase(createTemplate.fulfilled, (state, action) => {
            state.all = [...state.all, action.payload];
        });
        builder.addCase(patchTemplate.fulfilled, (state, action) => {
            const payload = action.payload;
            const index = state.all.findIndex(template => template.id == payload.id);
            if (index >= 0)
                state.all[index] = payload;
        });
        builder.addCase(deleteTemplate.fulfilled, (state, action) => {
            const arr = state.all.filter(template => template.id != action.payload);
            state.all = arr;
        });
    }
});

export default templatesSlice.reducer;