import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { postLogin, postLogout } from "../loader/loaderSlice";
import { getAllTemplates } from "../templates/templatesSlice";
import { getAllUsers } from "../users/usersSlice";

const initialState = {
    team: []
};

export const getTeamBoards = createAsyncThunk(
    'boards/getTeamBoards',
    async (data, thunkAPI) => {
        try {
            const state = thunkAPI.getState();
            if (!state.teams.joined)
                return thunkAPI.rejectWithValue(false);

            const response = await axios.get(`/api/boards?teamId=${state.teams.joined.id}&_embed=cards`);
            const usersAction = await thunkAPI.dispatch(getAllUsers());
            const users = usersAction.payload;

            const boards = response.data;
            boards.forEach(board => {
                board.cards.forEach(card => {
                    card.user = users.find(user => user.id == card.userId);
                });
                board.cards = board.cards.sort((card1, card2) => card2.rating - card1.rating);
            });


            return boards;
        } catch (error) {
            return thunkAPI.rejectWithValue(false);
        }
    }
);

export const createBoard = createAsyncThunk(
    'boards/createBoard',
    async (data, thunkAPI) => {
        try {
            const team = thunkAPI.getState().teams.joined;
            const templatesAction = await thunkAPI.dispatch(getAllTemplates());
            const templates = templatesAction.payload;
            const boardData = {
                name: data.name,
                teamId: team.id,
                columns: templates.find(template => template.id == data.template).columns
            };

            const response = await axios.post('/api/boards', boardData);
            return response.data;
        } catch (error) {
            return thunkAPI.rejectWithValue(false);
        }
    }
);

export const deleteBoard = createAsyncThunk(
    'boards/deleteBoard',
    async (id, thunkAPI) => {
        try {
            await axios.delete(`/api/boards/${id}`);
            await thunkAPI.dispatch(getTeamBoards());
            return id;
        } catch (error) {
            thunkAPI.rejectWithValue(false);
        }
    }
);

export const createCard = createAsyncThunk(
    'boards/createCard',
    async (data, thunkAPI) => {
        try {
            const state = thunkAPI.getState();
            const user = state.auth.user;
            const cardData = {
                ...data,
                userId: user.id,
                rating: 0
            };
            await axios.post('/api/cards', cardData);
            await thunkAPI.dispatch(getTeamBoards());
            return true;
        } catch (error) {
            thunkAPI.rejectWithValue(false);
        }
    }
);

export const deleteCard = createAsyncThunk(
    'boards/deleteCard',
    async (id, thunkAPI) => {
        try {
            await axios.delete(`/api/cards/${id}`);
            await thunkAPI.dispatch(getTeamBoards());
            return true;
        } catch (error) {
            thunkAPI.rejectWithValue(false);
        }
    }
);

export const addCardRating = createAsyncThunk(
    'boards/addCardRating',
    async (id, thunkAPI) => {
        try {
            const response = await axios.get(`/api/cards/${id}`);
            const card = response.data;

            await axios.patch(`/api/cards/${id}`, {rating: card.rating+1});
            await thunkAPI.dispatch(getTeamBoards());
            return true;
        } catch (error) {
            thunkAPI.rejectWithValue(false);
        }
    }
);


export const removeCardRating = createAsyncThunk(
    'boards/removeCardRating',
    async (id, thunkAPI) => {
        try {
            const response = await axios.get(`/api/cards/${id}`);
            const card = response.data;

            await axios.patch(`/api/cards/${id}`, {rating: card.rating-1});
            await thunkAPI.dispatch(getTeamBoards());
            return true;
        } catch (error) {
            thunkAPI.rejectWithValue(false);
        }
    }
);

export const boardsSlice = createSlice({
    name: 'boards',
    initialState: initialState,
    reducers: {
        unloadTeamBoards: state => {
            state.team = [];
        }
    },
    extraReducers: builder => {
        builder.addCase(getTeamBoards.fulfilled, (state, action) => {
            state.team = action.payload;
        });
        builder.addCase(createBoard.fulfilled, (state, action) => {
            state.team = [...state.team, action.payload];
        });
    }
});

export const { unloadTeamBoards } = boardsSlice.actions;
export default boardsSlice.reducer;

postLogin.push[getTeamBoards];
postLogout.push[unloadTeamBoards];