import { configureStore } from '@reduxjs/toolkit';
import authReducer, { authMiddleware, loadAuth } from './slices/auth/authSlice';
import boardsReducer from './slices/boards/boardsSlice';
import invitationsReducer from './slices/invitations/invitationsSlice';
import loaderReducer from './slices/loader/loaderSlice';
import teamsReducer from './slices/teams/teamsSlice';
import templatesReducer from './slices/templates/templatesSlice';
import usersReducer from './slices/users/usersSlice';

const store = configureStore({
    reducer: {
        loader: loaderReducer,
        auth: authReducer,
        teams: teamsReducer,
        users: usersReducer,
        invitations: invitationsReducer,
        templates: templatesReducer,
        boards: boardsReducer
    },
    preloadedState: {
        auth: loadAuth()
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(authMiddleware)
});

export default store;
