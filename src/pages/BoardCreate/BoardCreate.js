import React, { useCallback, useEffect } from 'react';
import { Button, Container, Form } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Navigation } from '../../components/Navigation/Navigation';
import RoundedContainer from '../../components/RoundedContainer/RoundedContainer';
import { createBoard } from '../../store/slices/boards/boardsSlice';
import { getAllTemplates } from '../../store/slices/templates/templatesSlice';

export default function BoardCreatePage() {
    const history = useHistory();
    const dispatch = useDispatch();
    const templates = useSelector(state => state.templates.all);
    const { register, handleSubmit, formState: { errors } } = useForm();

    useEffect(() => {
        dispatch(getAllTemplates());
    }, [dispatch]);

    const onSubmit = useCallback(data => {
        dispatch(createBoard(data)).then(() => {
            history.push('/boards/');
        });
    }, [dispatch, history]);

    return <React.Fragment>
        <Navigation />
        <Container>
            <RoundedContainer>
                <h1 className="display-4 my-3 text-center">Create a board</h1>
            </RoundedContainer>
            <Form onSubmit={handleSubmit(onSubmit)}>
                <RoundedContainer>
                    <Form.Group>
                        <Form.Label>Name:
                            {errors.name &&
                                <span className="fst-italic text-danger"> {errors.name.message}</span>
                            }
                        </Form.Label>
                        <Form.Control {...register('name', { required: "A name is required" })} />
                    </Form.Group>
                </RoundedContainer>
                <RoundedContainer>
                    <Form.Group>
                        <Form.Label>Template:
                            {errors.template &&
                                <span className="fst-italic text-danger"> {errors.template.message}</span>
                            }
                        </Form.Label>
                        <Form.Select
                            {...register('template', {
                                validate: val => val == "false" ? "A template is required" : true
                            })}
                        >
                            <option value={false}>- Choose a template -</option>
                            {templates.map(template =>
                                <option value={template.id} key={template.id}>{template.name}</option>
                            )}
                        </Form.Select>
                    </Form.Group>
                </RoundedContainer>
                <RoundedContainer>
                    <Button variant="primary" type="submit" className="w-100">Create</Button>
                </RoundedContainer>
            </Form>
        </Container>
    </React.Fragment>;
}