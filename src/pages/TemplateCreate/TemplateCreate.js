import React, { useCallback } from 'react';
import { Button, Container, Form } from 'react-bootstrap';
import { Controller, useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Navigation } from '../../components/Navigation/Navigation';
import RoundedContainer from '../../components/RoundedContainer/RoundedContainer';
import TemplateColumnsEditor from '../../components/TemplateColumnsEditor/TemplateColumnsEditor';
import { createTemplate } from '../../store/slices/templates/templatesSlice';
import { TemplateNameValidation, TemplateColumnsValidation } from '../../utility/validation/Template/TemplateValidation';

export default function TemplateCreatePage() {
    const history = useHistory();
    const dispatch = useDispatch();
    const { register, handleSubmit, control, formState: { errors } } = useForm();

    const onSubmit = useCallback(data => {
        dispatch(createTemplate(data)).then(() => {
            history.push('/templates');
        });
    }, [history, dispatch]);

    return <React.Fragment>
        <Navigation />
        <Container>

            <RoundedContainer>
                <h1 className="display-4 my-3 text-center">Create a template</h1>
            </RoundedContainer>
            <Form onSubmit={handleSubmit(onSubmit)}>
                <RoundedContainer>
                    <Form.Group>
                        <Form.Label>Name:
                            {errors.name && <span className="fst-italic text-danger"> {errors.name.message}</span>}
                        </Form.Label>
                        <Form.Control
                            {...register('name', TemplateNameValidation())}
                        />
                    </Form.Group>
                </RoundedContainer>
                <Controller
                    control={control}
                    name="columns"
                    defaultValue={[]}
                    render={({ field: { onChange, value } }) =>
                        <RoundedContainer>
                            {errors.columns && <span className="fst-italic text-danger"> {errors.columns.message}</span>}
                            <TemplateColumnsEditor columns={value} setColumns={onChange} />
                        </RoundedContainer>
                    }
                    rules={TemplateColumnsValidation()}
                />
                <RoundedContainer>
                    <Button variant="primary" className="w-100" type="submit">Create</Button>
                </RoundedContainer>
            </Form>
        </Container>
    </React.Fragment>;
}