import React, { useCallback, useEffect, useMemo } from 'react';
import { Col, Container, Row, ButtonGroup } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Navigation } from '../../components/Navigation/Navigation';
import RoundedContainer from '../../components/RoundedContainer/RoundedContainer';
import { deleteInvitation, getAllInvitations } from '../../store/slices/invitations/invitationsSlice';
import { join } from '../../store/slices/teams/teamsSlice';

export default function InvitationsPage() {
    const history = useHistory();
    const dispatch = useDispatch();
    const user = useSelector(state => state.auth.user);
    const invitations = useSelector(state => state.invitations.all);

    useEffect(() => {
        dispatch(getAllInvitations());
    }, [dispatch]);

    const filteredInvitaions = useMemo(() => {
        return invitations.filter(invitation => invitation.for == user.id);
    }, [invitations, user]);

    const onClickAccept = useCallback((invitation) => {
        dispatch(deleteInvitation(invitation.id));
        dispatch(join(invitation.team));
        history.replace('/teams');
    }, [dispatch, history]);

    const onClickDelete = useCallback(invitationId => {
        dispatch(deleteInvitation(invitationId));
    }, [dispatch]);

    return <React.Fragment>
        <Navigation />
        <Container>
            <RoundedContainer>
                <h1 className="display-4 my-3 text-center">Invitations</h1>
            </RoundedContainer>

            {filteredInvitaions.length == 0 &&
                <RoundedContainer>
                    <div className="text-center">No invitations</div>
                </RoundedContainer>
            }
            
            {filteredInvitaions.map(invitation =>
                <RoundedContainer key={invitation.id}>
                    <Row>
                        <Col xs="12" sm="6">
                            <span>By: <strong>{invitation.user.fullName}</strong></span>
                            <br />
                            <span>Team: <strong>{invitation.team.name}</strong></span>
                        </Col>
                        <Col xs="12" sm="6" className="d-flex align-items-center justify-content-center justify-content-sm-end">
                            <ButtonGroup>
                                <button className="btn text-success" onClick={() => onClickAccept(invitation)}>Accept</button>
                                <button className="btn text-danger"  onClick={() => onClickDelete(invitation.id)}>Delete</button>
                            </ButtonGroup>
                        </Col>
                    </Row>
                </RoundedContainer>
            )}
        </Container>
    </React.Fragment>;
}