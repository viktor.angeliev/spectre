import React from 'react';
import { Link } from 'react-router-dom';

export default function NotFoundPage() {
    return <div className="min-vh-100 d-flex justify-content-center align-items-center flex-column">
        <p className="display-2 ff-sonsie text-primary">Spectre</p>
        <p className="display-4">404</p>
        <h3>This page cannot be found</h3>
        <Link to="/">Go home</Link>
    </div>;
}