import React, { useCallback, useEffect, useMemo } from 'react';
import { Container, Nav, Tab } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory, useParams } from 'react-router-dom';
import EditableText from '../../components/EditableText/EditableText';
import { Navigation } from '../../components/Navigation/Navigation';
import TeamValidation from '../../utility/validation/Team/TeamValidation';
import { deleteTeam, getTeams, join, leave, patchTeam } from '../../store/slices/teams/teamsSlice';
import NotFoundPage from '../NotFound/NotFound';
import Icon from '../../components/Icon/Icon';
import CountCircle from '../../components/CountCircle/CountCircle';
import RowButton from '../../components/RowButton/RowButton';
import RoundedContainer from '../../components/RoundedContainer/RoundedContainer';

export default function TeamPage() {
    const { teamId } = useParams();
    const history = useHistory();
    const dispatch = useDispatch();
    const teams = useSelector(store => store.teams.all);
    const joined = useSelector(store => store.teams.joined);

    useEffect(() => {
        dispatch(getTeams());
    }, [dispatch]);

    const team = useMemo(() => {
        return teams.find(team => team.id == teamId);
    }, [teamId, teams]);

    const joinedId = useMemo(() => {
        return joined ? joined.id : null;
    }, [joined]);

    const onSaveTeamName = useCallback(form => {
        const data = { name: form.text };
        dispatch(patchTeam({ id: team.id, data: data }));
    }, [dispatch, team]);

    const onClickDelete = useCallback(() => {
        if (!confirm("Are you sure you want to delete this team?"))
            return;

        dispatch(deleteTeam(teamId)).then(() => {
            history.push('/teams');
        });
    }, [dispatch, history, teamId]);

    const onClickJoin = useCallback(() => {
        dispatch(join(team));
    }, [dispatch, team]);

    const onClickLeave = useCallback(() => {
        dispatch(leave());
    }, [dispatch]);

    if (!team)
        return <NotFoundPage />;

    return <React.Fragment>
        <Navigation />
        {team &&
            <Container>
                <RoundedContainer>
                    <EditableText
                        text={team.name}
                        onSave={onSaveTeamName}
                        validation={TeamValidation}
                        wrapper={<h1 className="display-4 my-3 text-center" />}
                    />
                </RoundedContainer>
                <RoundedContainer>
                    <Tab.Container defaultActiveKey="members">
                        <Nav variant="pills" className="flex-column flex-sm-row gap-2">
                            <Nav.Item>
                                <Nav.Link eventKey="members">Members <CountCircle value={team.users.length} /></Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="boards">Boards</Nav.Link>
                            </Nav.Item>
                        </Nav>
                        <Tab.Content>
                            <Tab.Pane eventKey="members">
                                {team.users.map(user =>
                                    <RowButton to={`/users/${user.id}`} key={user.id} icon={Icon.Person}>
                                        {user.fullName}
                                    </RowButton>
                                )}
                            </Tab.Pane>
                            <Tab.Pane eventKey="boards">
                                WIP
                            </Tab.Pane>
                        </Tab.Content>
                    </Tab.Container>
                </RoundedContainer>
                <RoundedContainer className="d-flex gap-2">
                    {team.id === joinedId
                        && <Link className="btn text-primary" to="/teams/invite">Invite</Link>
                    }
                    {!joined
                        && <button className="btn text-primary" onClick={onClickJoin}>Join</button>
                    }
                    {team.id === joinedId
                        && <button className="btn text-danger" onClick={onClickLeave}>Leave</button>
                    }
                    <button className="btn text-danger ms-auto" onClick={onClickDelete}>Delete</button>
                </RoundedContainer>
            </Container>
        }
    </React.Fragment>;
}