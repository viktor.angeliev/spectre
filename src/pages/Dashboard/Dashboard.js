import React from 'react';
import { Container } from 'react-bootstrap';
import { Navigation } from '../../components/Navigation/Navigation';
import RoundedContainer from '../../components/RoundedContainer/RoundedContainer';

export default function DashboardPage(){
    return <div>
        <Navigation />
        <Container>
            <RoundedContainer>
                Welcome to the spectre dashboard
            </RoundedContainer>
        </Container>
    </div>;
}