import React from "react";
import { Link } from "react-router-dom";
import LoginForm from "../../components/LoginForm/LoginForm";

export default function LoginPage() {
    return <div className="min-vh-100 d-flex justify-content-center align-items-center flex-column">
        <h1 className="text-center my-5">
            <span className="display-4">Welcome to</span>
            <br />
            <span className="display-1 ff-sonsie text-primary">
                <Link to="/" className="text-decoration-none">Spectre</Link>
            </span>
        </h1>
        <div className="bg-white p-5 shadow rounded border">
            <p className="fw-bold">Login</p>
            <LoginForm />
            <span className="text-muted mt-3 d-block">
                <span>Don't have an account? </span>
                <Link to="/register">Register</Link>
            </span>
        </div>
    </div>;
}