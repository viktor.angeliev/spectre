import React, { useCallback, useEffect } from 'react';
import { Button, Container, Form } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Navigation } from '../../components/Navigation/Navigation';
import RoundedContainer from '../../components/RoundedContainer/RoundedContainer';
import { createTeam, getTeams } from '../../store/slices/teams/teamsSlice';
import TeamValidation from '../../utility/validation/Team/TeamValidation';

export default function TeamCreatePage() {
    const history = useHistory();
    const dispatch = useDispatch();
    const teams = useSelector(state => state.teams.all);
    const { register, handleSubmit, formState: { errors } } = useForm();

    useEffect(() => {
        dispatch(getTeams());
    }, [dispatch]);

    const onSubmit = useCallback(data => {
        dispatch(createTeam({ name: data.name })).then(() => {
            history.push('/teams');
        });
    }, [dispatch, history]);

    return <React.Fragment>
        <Navigation />
        <Container>
            <RoundedContainer>
                <h1 className="display-4 my-3 text-center">Create a team</h1>
            </RoundedContainer>
            <RoundedContainer>
                <Form onSubmit={handleSubmit(onSubmit)}>
                    <Form.Group className="mb-2">
                        <Form.Label htmlFor="name">
                            Name:
                            {errors.name && <span className="text-danger fst-italic m-0"> {errors.name.message}</span>}
                        </Form.Label>
                        <Form.Control
                            className={errors.name && 'border-danger'}
                            {...register("name", TeamValidation(teams))}
                            autoComplete="name"
                        />
                    </Form.Group>

                    <Button className="w-100" variant="primary" type="submit">Create</Button>
                </Form>
            </RoundedContainer>
        </Container>
    </React.Fragment>;
}