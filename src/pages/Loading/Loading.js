import React from 'react';

export default function LoadingPage() {
    return <div className="vw-100 vh-100 d-flex justify-content-center align-items-center">
        <h1 className="m-auto">Loading....</h1>
    </div>;
}