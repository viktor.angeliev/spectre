import React, { useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import Icon from '../../components/Icon/Icon';
import { Navigation } from '../../components/Navigation/Navigation';
import RoundedContainer from '../../components/RoundedContainer/RoundedContainer';
import RowButton from '../../components/RowButton/RowButton';
import { getAllTemplates } from '../../store/slices/templates/templatesSlice';

export default function TemplatesPage() {
    const dispatch = useDispatch();
    const templates = useSelector(state => state.templates.all);

    useEffect(() => {
        dispatch(getAllTemplates());
    }, [dispatch]);

    return <React.Fragment>
        <Navigation />
        <Container>
            <RoundedContainer>
                <h1 className="display-4 my-3 text-center">Templates</h1>
            </RoundedContainer>
            <RoundedContainer>
                <RowButton className="m-0" icon={Icon.Add} to="/templates/create">Create template</RowButton>
                {templates.map(template =>
                    <RowButton key={template.id} to={`/templates/${template.id}`} icon={Icon.Columns}>
                        {template.name}
                    </RowButton>
                )}
            </RoundedContainer>
        </Container>
    </React.Fragment>;
}