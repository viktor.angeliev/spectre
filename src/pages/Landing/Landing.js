import React from 'react';
import { Link } from 'react-router-dom';
import RegisterForm from '../../components/RegisterForm/RegisterForm';

export default function LandingPage() {
    return <div className="min-vh-100 d-flex justify-content-center align-items-center flex-column">
        <h1 className="text-center my-5">
            <span className="display-4">Welcome to</span>
            <br />
            <span className="display-1 ff-sonsie text-primary">
                <Link to="/" className="text-decoration-none">Spectre</Link>
            </span>
        </h1>
        <div className="d-flex align-items-stretch flex-column flex-md-row">
            <div className="bg-white p-5 shadow rounded border">
                <p className="fw-bold">Start by creating an account</p>
                <RegisterForm />
            </div>
            <div className="bg-primary px-5 my-md-5 py-md-0 py-5 text-center text-white
                            d-flex flex-column justify-content-center align-items-center
                            shadow rounded-end
                            ">
                <h3 className="mb-3">Already have an account?</h3>
                <Link to="/login" className="btn btn-outline-light w-75">Login</Link>
            </div>
        </div>
    </div>;
}