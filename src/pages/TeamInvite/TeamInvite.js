import React, { useCallback, useEffect, useMemo } from 'react';
import { Container } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import Icon from '../../components/Icon/Icon';
import { Navigation } from '../../components/Navigation/Navigation';
import RoundedContainer from '../../components/RoundedContainer/RoundedContainer';
import RowButton from '../../components/RowButton/RowButton';
import { createInvitation, deleteInvitation, getAllInvitations } from '../../store/slices/invitations/invitationsSlice';
import { getAllUsers } from '../../store/slices/users/usersSlice';
import NotFoundPage from '../NotFound/NotFound';

export default function TeamInvitePage() {
    const dispatch = useDispatch();
    const joined = useSelector(state => state.teams.joined);
    const user = useSelector(state => state.auth.user);
    const users = useSelector(state => state.users.all);
    const invitations = useSelector(state => state.invitations.all);

    useEffect(() => {
        dispatch(getAllUsers());
        dispatch(getAllInvitations());
    }, [dispatch]);

    const filteredUsers = useMemo(() => {
        return users.filter(user => user.teamId == null);
    }, [users]);

    const onClickUser = useCallback((forId, invitation) => {
        if (invitation)
            dispatch(deleteInvitation(invitation.id));
        else {
            const data = {
                for: forId,
                userId: user.id,
                teamId: joined.id
            };
            dispatch(createInvitation(data));
        }
    }, [dispatch, joined, user]);

    if (!joined)
        return <NotFoundPage />;

    return <React.Fragment>
        <Navigation />
        <Container>
            <RoundedContainer>
                <h1 className="display-4 my-3 text-center">Invite a member</h1>
            </RoundedContainer>
            <RoundedContainer>
                <span>Your team: <Link to={`/teams/${joined.id}`}>{joined.name}</Link></span>
            </RoundedContainer>
            <RoundedContainer>
                {filteredUsers.map((user, index) => {
                    const invitation = invitations.find(invitation => invitation.for === user.id);
                    const className = `${index == 0 ? 'mt-0' : ''} ${invitation ? 'bg-success text-white' : ''}`;
                    return <RowButton
                        key={user.id}
                        tag="div"
                        icon={Icon.Person}
                        onClick={() => onClickUser(user.id, invitation)}
                        className={className}>
                        {user.fullName}
                    </RowButton>;
                })}
            </RoundedContainer>
        </Container>
    </React.Fragment>;
}