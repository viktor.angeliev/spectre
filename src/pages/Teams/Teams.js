import React from 'react';
import { Container } from 'react-bootstrap';
import Icon from '../../components/Icon/Icon';
import { Navigation } from '../../components/Navigation/Navigation';
import RoundedContainer from '../../components/RoundedContainer/RoundedContainer';
import RowButton from '../../components/RowButton/RowButton';
import TeamsList from '../../components/TeamsList/TeamsList';

export default function TeamsPage() {
    return <React.Fragment>
        <Navigation />
        <Container>
            <RoundedContainer>
                <h1 className="display-4 my-3 text-center">Teams</h1>
            </RoundedContainer>
            <RoundedContainer>
                <RowButton className="m-0" to="/teams/create" icon={Icon.Add}>Create team</RowButton>
                <TeamsList />
            </RoundedContainer>
        </Container>
    </React.Fragment>;
}