import React, { useCallback, useEffect, useMemo } from 'react';
import { Container } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import BoardColumn from '../../components/BoardColumn/BoardColumn';
import { Navigation } from '../../components/Navigation/Navigation';
import { deleteBoard, getTeamBoards } from '../../store/slices/boards/boardsSlice';
import './Board.scss';

export default function BoardPage() {
    const { boardId } = useParams();
    const history = useHistory();
    const dispatch = useDispatch();
    const boards = useSelector(state => state.boards.team);

    useEffect(() => {
        dispatch(getTeamBoards());
    }, [dispatch]);

    const board = useMemo(() => {
        return boards.find(board => board.id == boardId);
    }, [boards, boardId]);

    const onClickDelete = useCallback(() => {
        if (confirm("Are you sure you want to delete this boards?")) {
            dispatch(deleteBoard(boardId)).then(() => {
                history.push('/boards');
            });
        }
    }, [dispatch, boardId, history]);

    return <React.Fragment>
        <Navigation />
        {board &&
            <React.Fragment>
                <Container>
                    <h1 className="display-4 my-4 text-center">{board.name}</h1>
                </Container>
                <div className="board-column-wrapper">
                    <div className="board-column-container">
                        {board.columns.map(column =>
                            <BoardColumn key={column.id} column={column} board={board} />
                        )}
                    </div>
                </div>
                <Container className="d-flex">
                    <button className="btn text-danger ms-auto" onClick={onClickDelete}>Delete</button>
                </Container>
            </React.Fragment>
        }
    </React.Fragment>;
}