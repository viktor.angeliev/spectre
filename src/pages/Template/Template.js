import React, { useCallback, useEffect, useMemo } from 'react';
import { Container, Form } from 'react-bootstrap';
import { Controller, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import EditableText from '../../components/EditableText/EditableText';
import { Navigation } from '../../components/Navigation/Navigation';
import RoundedContainer from '../../components/RoundedContainer/RoundedContainer';
import TemplateColumnsEditor from '../../components/TemplateColumnsEditor/TemplateColumnsEditor';
import { deleteTemplate, getAllTemplates, patchTemplate } from '../../store/slices/templates/templatesSlice';
import { TemplateColumnsValidation, TemplateNameValidation } from '../../utility/validation/Template/TemplateValidation';

export default function TemplatePage() {
    const history = useHistory();
    const { templateId } = useParams();
    const dispatch = useDispatch();
    const templates = useSelector(state => state.templates.all);
    const { handleSubmit, reset, control, formState: { errors, isDirty } } = useForm();

    useEffect(() => {
        dispatch(getAllTemplates());
    }, [dispatch]);

    const template = useMemo(() => {
        return templates.find(template => template.id == templateId);
    }, [templateId, templates]);

    useEffect(() => {
        if (!template)
            return;
        reset({ columns: template.columns });
    }, [reset, template]);

    const onSaveTemplateName = useCallback(data => {
        dispatch(patchTemplate({ id: templateId, data: { name: data.text } }));
    }, [dispatch, templateId]);

    const onSubmit = useCallback(data => {
        dispatch(patchTemplate({ id: templateId, data }));
    }, [dispatch, templateId]);

    const onClickReset = useCallback(() => {
        reset({ columns: template.columns });
    }, [template, reset]);

    const onClickDelete = useCallback(() => {
        if (confirm("Are you sure you want to delete this template?")){
            dispatch(deleteTemplate(templateId)).then(() => {
                history.push('/templates');
            });
        }
    }, [dispatch, history, templateId]);

    return <React.Fragment>
        <Navigation />
        {template &&
            <Container>
                <RoundedContainer>
                    <EditableText
                        text={template.name}
                        onSave={onSaveTemplateName}
                        validation={TemplateNameValidation()}
                        wrapper={<h1 className="display-4 my-3 text-center" />}
                    />
                </RoundedContainer>
                <Form onSubmit={handleSubmit(onSubmit)}>
                    <RoundedContainer>
                        <Controller
                            control={control}
                            name="columns"
                            defaultValue={template.columns}
                            render={({ field: { onChange, value } }) =>
                                <RoundedContainer>
                                    {errors.columns && <span className="fst-italic text-danger"> {errors.columns.message}</span>}
                                    <TemplateColumnsEditor columns={value} setColumns={onChange} />
                                </RoundedContainer>
                            }
                            rules={TemplateColumnsValidation()}
                        />
                    </RoundedContainer>
                    <RoundedContainer className="d-flex">
                        {isDirty && <button className="btn text-primary" type="submit">Save</button>}
                        {isDirty && <button className="btn" type="button" onClick={onClickReset}>Reset</button>}
                        <button className="btn text-danger ms-auto" type="button" onClick={onClickDelete}>Delete</button>
                    </RoundedContainer>
                </Form>
            </Container>
        }
    </React.Fragment>;
}