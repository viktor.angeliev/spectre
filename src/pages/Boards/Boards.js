import React, { useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import Icon from '../../components/Icon/Icon';
import { Navigation } from '../../components/Navigation/Navigation';
import RoundedContainer from '../../components/RoundedContainer/RoundedContainer';
import RowButton from '../../components/RowButton/RowButton';
import { getTeamBoards } from '../../store/slices/boards/boardsSlice';

export default function BoardsPage() {
    const dispatch = useDispatch();
    const joined = useSelector(state => state.teams.joined);
    const boards = useSelector(state => state.boards.team);

    useEffect(() => {
        dispatch(getTeamBoards());
    }, [dispatch]);

    return <React.Fragment>
        <Navigation />
        {joined &&
            <Container>
                <RoundedContainer>
                    <h1 className="display-4 my-3 text-center">Boards</h1>
                </RoundedContainer>
                <RoundedContainer>
                    <RowButton className="mt-0" icon={Icon.Add} to="/boards/create">
                        Create board
                    </RowButton>
                    {boards.map(board =>
                        <RowButton icon={Icon.Board} to={`/boards/${board.id}`} key={board.id}>
                            {board.name}
                        </RowButton>
                    )}
                </RoundedContainer>
            </Container>
        }
        {!joined &&
            <Container>
                <RoundedContainer className="text-center">
                    <span>You must join a team before you can view boards.</span>
                </RoundedContainer>
            </Container>
        }
    </React.Fragment>;
}