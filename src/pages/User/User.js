import {Navigation} from "../../components/Navigation/Navigation";
import {Container} from "react-bootstrap";
import RoundedContainer from "../../components/RoundedContainer/RoundedContainer";
import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getUser} from "../../store/slices/users/usersSlice";
import {useParams} from "react-router-dom";

export default function User() {
    const dispatch = useDispatch();
    const {userId} = useParams();
    const user = useSelector(state => state.users.user);

    useEffect(() => {
        dispatch(getUser(userId));
    }, [dispatch, userId]);

    return <React.Fragment>
        <Navigation/>
        <Container>
            <RoundedContainer>
                <h1 className="display-4 my-3 text-center">User</h1>
            </RoundedContainer>
            <RoundedContainer>
                {user && <React.Fragment>
                    <h2>{user.fullName}</h2>
                    <p className='m-0'>{user.email}</p>
                </React.Fragment>}
            </RoundedContainer>
        </Container>
    </React.Fragment>;
}