import React from "react";
import { Link } from "react-router-dom";
import RegisterForm from "../../components/RegisterForm/RegisterForm";

export default function RegisterPage() {
    return <div className="min-vh-100 d-flex justify-content-center align-items-center flex-column">
        <h1 className="text-center my-5">
            <span className="display-4">Welcome to</span>
            <br />
            <span className="display-1 ff-sonsie text-primary">
                <Link to="/" className="text-decoration-none">Spectre</Link>
            </span>
        </h1>
        <div className="bg-white p-5 shadow rounded border">
            <p className="fw-bold">Create an account</p>
            <RegisterForm />
            <span className="text-muted mt-3 d-block">
                <span>Already have an account? </span>
                <Link to="/login">Login</Link>
            </span>
        </div>
    </div>;
}