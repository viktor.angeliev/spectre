const path = require('path');
const jsonServer = require('json-server');
const auth = require('json-server-auth');
const fs = require('fs');
const jwt = require('jsonwebtoken');
const express = require('express');
const { JWT_SECRET_KEY } = require('json-server-auth/dist/constants');
const { resolveSoa } = require('dns');

const app = jsonServer.create();
const router = jsonServer.router('db.json');

const rules = auth.rewriter({
  users: 664
});

app.db = router.db;
app.use('/api/users/:id', express.json());


//Overwrite json-server-auth put
app.put('/api/users/:id', (req, res, next) => {
  try {
    const user = req.body;
    const id = Number.parseInt(req.params.id);
    const index = app.db.get('users').findIndex({ id: id }).value();
    const users = app.db.get('users').value();
    users[index] = user;
    app.db.get('users').assign(users).write();
    res.status(200).send();
  } catch (error) {
    res.status(400).send();
  }
});


app.use("/api", rules);
app.use("/api", auth);

app.post('/api/reauthenticate', (req, res, next) => {
  try {
    const token = req.body.auth_token;
    const decoded = jwt.verify(token, JWT_SECRET_KEY);
    const user = app.db.get('users').find({ email: decoded.email }).value();
    res.json(user);
  } catch (error) {
    res.status(401).send();
  }
});



app.use("/api", router);

//Used for react router
app.get('*', (req, res) => {
  const root = path.join(__dirname + "/public");
  const p = path.join(root + req.path);

  if (fs.existsSync(p))
    return res.sendFile(p);

  return res.sendFile(path.join(root + '/index.html'));
});


app.listen(3000, 'localhost', () => {
  console.log('JSON Server is running');
});